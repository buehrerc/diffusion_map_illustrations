import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

from diffusion_map import DiffusionMap


class Animator:
    def __init__(self, matrix, epochs, title='Animated Diffusion Map'):
        self.num_epochs = epochs
        self.matrix = matrix
        self.dm = DiffusionMap()
        self.limits = self._get_limits()
        # Create Animation
        self.fig, self.ax = plt.subplots()
        self.fig.suptitle(title)
        self.anim = FuncAnimation(fig=self.fig,
                                  func=self.update,
                                  frames=epochs,
                                  interval=500,
                                  init_func=self.init,
                                  blit=True)

    def _get_limits(self):
        """Function sets the limits of the scatter plot by assuming that the first epoch has the largest values."""
        # Currently, I assume that the last epoch has the widest limits
        xy_last_epoch = self.dm.transform(self.matrix, dim=2, t=1)
        boundaries = np.max(np.abs(xy_last_epoch), axis=0)
        # Add a puffer of 0.1 to boundaries
        return sum(list(map(lambda e: [e*-1.1, e*1.1], boundaries)), [])

    def init(self):
        """Function initializes the first frame of the animation."""
        return self.update(0)

    def update(self, i):
        """Function retieves the next frame of the animation."""
        self.ax.clear()
        self.ax.axis(self.limits)

        xy = self.dm.transform(self.matrix, dim=2, t=i+1)
        # It may happend that the returned values have imaginary parts as well
        # -> we are only interested in the real part
        xy = np.real(xy)
        scatter = self.ax.scatter(xy[:, 0], xy[:, 1])
        return scatter,

    def save(self, path):
        self.anim.save(path, writer='pillow')


if __name__ == '__main__':
    from graph_database import GraphDatabase
    graph_db = GraphDatabase()
    graph = graph_db.get_graph('random', nodes=50)
    animator = Animator(graph, epochs=5, title='Random Graph (p=0.2) Diffusion Map')
    animator.save(r'./data/random_graph_100.gif')
