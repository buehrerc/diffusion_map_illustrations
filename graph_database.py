import numpy as np


class GraphDatabase:
    def __init__(self):
        self.petersen = lambda n: self._generate_petersen_graph(n)
        self.star = lambda n: self._generate_star_graph(n)
        self.circle = lambda n: self._generate_circle_graph(n)
        self.random = lambda n: self._generate_random_graph(n)
        self.sbm = lambda n: self._generate_sbm_graph(n)
        self.supported_graphs = ['petersen', 'star', 'circle', 'random']

    @staticmethod
    def _generate_petersen_graph(nodes):
        return np.array([[0, 0, 1, 1, 0, 1, 0, 0, 0, 0],
                         [0, 0, 0, 1, 1, 0, 1, 0, 0, 0],
                         [1, 0, 0, 0, 1, 0, 0, 1, 0, 0],
                         [1, 1, 0, 0, 0, 0, 0, 0, 1, 0],
                         [0, 1, 1, 0, 0, 0, 0, 0, 0, 1],
                         [1, 0, 0, 0, 0, 0, 1, 0, 0, 1],
                         [0, 1, 0, 0, 0, 1, 0, 1, 0, 0],
                         [0, 0, 1, 0, 0, 0, 1, 0, 1, 0],
                         [0, 0, 0, 1, 0, 0, 0, 1, 0, 1],
                         [0, 0, 0, 0, 1, 1, 0, 0, 1, 0]])

    @staticmethod
    def _generate_star_graph(nodes):
        x = np.ones(shape=(nodes, nodes))
        np.fill_diagonal(x, 0)
        return x

    @staticmethod
    def _generate_circle_graph(nodes):
        x_first_row = np.zeros(shape=(nodes,))
        x_first_row[1] = 1
        x_first_row[-1] = 1
        return np.array([np.roll(x_first_row, shift=shift, axis=0) for shift in range(nodes)])

    @staticmethod
    def _generate_random_graph(nodes):
        x = np.array((np.abs(np.random.normal(0, 1, size=(nodes, nodes))) > 1), dtype=int)
        np.fill_diagonal(x, 0)
        return x

    @staticmethod
    def _generate_sbm_graph(nodes):
        # w.l.o.g. we assume that the first split entries belong to the same community.
        split, p, q = 0.5, 0.8, 0.2
        matrix = np.random.uniform(0, 1, size=(nodes, nodes))
        # Set the nodes based on the fixed parameters
        split_mask = np.arange(nodes) < np.floor(nodes*split)
        mask = np.transpose([split_mask]) * split_mask + np.transpose([~split_mask]) * ~split_mask
        mask = mask * (1-p) + ~mask * (1-q)
        matrix = np.array(matrix >= mask, dtype=int)
        np.fill_diagonal(matrix, 0)
        matrix = np.triu(matrix)
        matrix += np.triu(matrix).T
        return matrix

    def get_graph(self, graph='petersen', nodes=5):
        """
        Returns the adjacency matrix of input graph.
        Supported graphs:
        - petersen
        - star
        - circle
        - sbm (Stochastic Block Model)
        :return: adjacency matrix
        """
        return getattr(self, graph)(nodes)


if __name__ == '__main__':
    graph_db = GraphDatabase()
    graph_db.get_graph('sbm', 100)
