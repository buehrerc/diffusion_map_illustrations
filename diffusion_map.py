import logging
import numpy as np
from scipy import linalg


class DiffusionMap:
    """
    Class computes the diffusion maps of adjacency matrices
    Note that matrix notation was copied from the lecture.
    """
    def __init__(self):
        pass

    @staticmethod
    def _generate_decomposition(adjacency_matrix):
        # 1) Transform the adjacency matrix to transition probability matrix M = inv(D)*W
        D = np.eye(adjacency_matrix.shape[0]) * np.sum(adjacency_matrix, axis=1)
        M = adjacency_matrix * np.transpose([1 / np.sum(adjacency_matrix, axis=1)])
        # 2) Get symmetric matrix S
        S = np.dot(linalg.sqrtm(D), np.dot(M, linalg.inv(linalg.sqrtm(D))))
        # 3) Get spectral decomposition of S (eigenvalues and eigenvectors)
        A, V = linalg.eig(S)
        # 3.1) Order eigenvalues in descending order
        order = np.flip(np.argsort(A))
        A = A[order]
        V = V[:, order]
        # 4) Get the biorthogonal system
        phi = np.dot(linalg.inv(linalg.sqrtm(D)), V)
        psi = np.dot(linalg.sqrtm(D), V)
        # 5) Check whether reconstruction error is within tolerance
        reconstruction_error = np.abs(np.sum(M - np.dot(phi, np.dot(np.eye(A.shape[0]) * A, np.transpose(psi)))))
        if reconstruction_error > 1e-14:
            logging.warn(
                'Reconstruction of input adjacency matrix was not within tolerances: {}'.format(reconstruction_error))
        return phi, A

    @staticmethod
    def _generate_diffusion_map(phi, A, dim, t):
        return np.power(A[1:dim+1], t)*phi[:, 1:dim+1]

    def transform(self, adjacency_matrix, dim, t=1):
        """
        Function takes input adjacency matrix and generates its diffusion map.
        :param adjacency_matrix: Input Matrix
        :param dim: Number of dimensions to extract from the diffusion map
        :param t: Point in time, where diffusion maps should be attained
        :return: Diffusion Map at point t
        """
        # 1) Generate diffusion map decomposition
        phi, A = self._generate_decomposition(adjacency_matrix)
        # 2) Generate diffusion map based on dim and t
        diffusion_map = self._generate_diffusion_map(phi, A, dim, t)
        return diffusion_map


if __name__ == '__main__':
    from graph_database import GraphDatabase

    graph_db = GraphDatabase()
    matrix = graph_db.get_graph('circle', nodes=8)
    dm = DiffusionMap()
    diff_map_ = dm.transform(matrix, 2)
    print(diff_map_)