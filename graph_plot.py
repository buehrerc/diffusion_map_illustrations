import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from graph_database import GraphDatabase
from diffusion_map import DiffusionMap


class GraphPlotter:
    def __init__(self):
        self.graph_db = GraphDatabase()
        self.dm = DiffusionMap()

    def plot_3d(self, graph, nodes):
        """
        Plots graph and its diffusion map side-by-side.
        The following graphs are supported:
        - petersen
        - circle
        - star
        - random
        - user_input
        :param graph: String of graph name or matrix
        :param nodes: number of nodes the string should contain
        :return: matplotlib figure
        """
        # 1) Get adjacency matrix of graph
        adjacency_matrix = self.graph_db.get_graph(graph, nodes)
        # 2) Generate graph
        graph_plot = self._generate_graph(adjacency_matrix)
        # 3) Generate diffusion map
        diff_map = self.dm.transform(adjacency_matrix, dim=3, t=1)
        # 4) Generate matplotlib figure
        fig = self._generate_figure_3d(graph_plot, diff_map)
        return fig

    def plot_2d(self, graph, nodes):
        """
        Plots graph and its diffusion map side-by-side.
        The following graphs are supported:
        - petersen
        - circle
        - star
        - random
        - user_input
        :param graph: String of graph name or matrix
        :param nodes: number of nodes the string should contain
        :return: matplotlib figure
        """
        # 1) Retrieve adjacency matrix
        if isinstance(graph, str):
            adjacency_matrix = self.graph_db.get_graph(graph, nodes)
        elif isinstance(graph, np.ndarray):
            assert self._assert_valid_adjacency_matrix(graph), 'Not a valid adjacency matrix'
            adjacency_matrix = graph
        else:
            raise TypeError('Unknown input type!')
        # 2) Generate graph
        graph_plot = self._generate_graph(adjacency_matrix)
        # 3) Generate diffusion map
        diff_map = self.dm.transform(adjacency_matrix, dim=2, t=1)
        # 4) Generate matplotlib figure
        fig = self._generate_figure_2d(graph_plot, diff_map)
        return fig

    def get_graph(self, matrix):
        """
        Functions generates a graph using the input matrix.
        :param matrix: np.ndarray
        :return: networkx figure
        """
        # Get networkx object
        graph_plot = self._generate_graph(matrix)
        return graph_plot

    @staticmethod
    def _assert_valid_adjacency_matrix(matrix):
        """Check whether matrix is a valid adjacency matrix"""
        return np.all([matrix.shape[0] == matrix.shape[1],
                       np.all(matrix == np.transpose(matrix))])

    @staticmethod
    def _generate_figure_3d(graph_plot, diff_map):
        fig = plt.figure()
        ax_graph = fig.add_subplot(121)
        nx.draw(graph_plot, ax=ax_graph)
        ax_diff_map = fig.add_subplot(122, projection='3d')
        ax_diff_map.scatter(diff_map[:, 0], diff_map[:, 1], diff_map[:, 2])
        return fig

    @staticmethod
    def _generate_figure_2d(graph_plot, diff_map):
        fig, ax = plt.subplots(1, 2)
        nx.draw(graph_plot, ax=ax[0])
        ax[1].scatter(diff_map[:, 0], diff_map[:, 1])
        return fig

    @staticmethod
    def _generate_graph(adjacency_matrix):
        """Function generates a graph using the input adjacency matrix"""
        assert adjacency_matrix.shape[0] == adjacency_matrix.shape[1], 'Not an adjacency matrix'
        graph = nx.Graph()
        # Add nodes
        for n in range(len(adjacency_matrix)):
            graph.add_node(n)
        # Add edges
        for i, j in tuple(zip(*np.where(adjacency_matrix != 0))):
            graph.add_edge(i, j, weight=adjacency_matrix[i, j])
        return graph


if __name__ == '__main__':
    gp = GraphPlotter()
    g = gp.plot_3d('circle', 5)
