networkx==2.6.3
notebook==6.4.6
numpy==1.21.4
matplotlib==3.5.0
pandas==1.3.4
scipy==1.7.3